# jkoan

## Koan compilation in json
This is an open compilation of Koan with author and country of origin in **JSON** for open projects.

## What is a Koan?
A Koan (公案) are problems introduced to a zen student for the master to check their progression. 

![](https://www.northcountrypublicradio.org/news/images/Yoshitoshi_-_100_Aspects_of_the_Moon_-_74.jpg)

## Our sources
See more abour our resources in the [wiki](https://gitlab.com/delightfulagony/jkoan/wikis/Resources).

