import json
import sys

if __name__== "__main__":
  with open("Koan/koan.json", "r") as read_file:
    koans = json.load(read_file)

  for i in koans:
    for j in i.keys():
      if j != "title" and j != "author" and j != "origin" and j!="koan":
        sys.exit(1)

  sys.exit(0)
