import json
from difflib import SequenceMatcher
import sys

if __name__ == '__main__':
  with open("Koan/koan.json", "r") as read_file:
    koans = json.load(read_file)

  for i in range(len(koans)):
    for j in range(i+1, len(koans)):
      if ((SequenceMatcher(None, koans[i]["title"], koans[j]["title"]).ratio() > 0.80) or (SequenceMatcher(None, koans[i]["koan"], koans[j]["koan"]).ratio() > 0.75)):
        sys.exit(1)

  sys.exit(0)
